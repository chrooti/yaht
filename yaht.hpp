#ifndef YAHT
#define YAHT

// TODO: review naming and comments

#include <stddef.h>
#include <stdint.h>

#include <type_traits>

#define YAHT_ASSERT_IS_POD(...)                       \
    static_assert(                                    \
        ::std::is_standard_layout<__VA_ARGS__>::value \
            && ::std::is_trivial<__VA_ARGS__>::value, \
        ""                                            \
    );

#if defined(__clang__) || defined(__GNUC__)
#define YAHT_INLINE static inline __attribute__((__always_inline__, __unused__))
#define YAHT_LIKELY(x) __builtin_expect(!!(x), 1)
#define YAHT_UNLIKELY(x) __builtin_expect(!!(x), 0)
#define YAHT_ATTRIBUTE(x) __attribute__(x)
#elif defined(_MSC_VER)
// clang-format off
#define YAHT_INLINE static inline __forceinline
// clang-format on
#define YAHT_LIKELY(x) (x)
#define YAHT_UNLIKELY(x) (x)
#define YAHT_ATTRIBUTE(x)
#else
// clang-format off
#define YAHT_INLINE static inline
// clang-format on
#define YAHT_LIKELY(x) (x)
#define YAHT_UNLIKELY(x) (x)
#define YAHT_ATTRIBUTE(x)
#endif

namespace yaht {

/* ALLOCATOR */

struct Allocator {
    void* (*allocate)(size_t);
    void* (*reallocate)(void*, size_t);
    void (*deallocate)(void*);
};

/* TABLE */

namespace detail {

// A node fingerprint is composed as follows:
// - the upper 16 bit contain the distance of the node from it's ideal position *plus one* (overflowing is not a concern with any decent hash)
// - the lower 16 bit are the lower 16 bit of the hash
using Fingerprint = uint32_t;
constexpr uint32_t HASH_LOWER_BITS = 16;
constexpr uint32_t FINGERPRINT_DISTANCE_UNIT = 1 << HASH_LOWER_BITS;

struct Metadata {
    uint32_t item_index;
    detail::Fingerprint fingerprint;
};

} // namespace detail

constexpr uint64_t FOUND_BIT = 1L << 32;

template<typename Item>
struct Node {
    Item item;
    uint32_t metadata_index;
};

template<
    // Type of a single table element
    typename Item,

    // struct that contains a single static method: size_t call(K a) that hashes the key
    typename Hash,

    // struct that contains a single static method: bool call(K a, K b) that returns true if the keys are equal, false otherwise
    typename Pred>
struct Table {
    // item struct for both map and set, which are actually syntax sugar for:
    // - Set: a table with a node with a key
    // - Map: a table with a node with a key and a value

    // number of inserted elements
    uint32_t size;

    // size before rehashing (max_size * load_factor)
    uint32_t rehash_size;

    // number of max element inserted (must be a power of 2)
    uint32_t max_size;

    // max_size - 1
    uint32_t size_mask;

    // load factor (integer between 0 and 100)
    uint32_t load_factor;

    // container for allocation function
    Allocator allocator;

    // pointer to the nodes
    Node<Item>* nodes;
};

// TODO: decent interface to this
template<typename Item>
struct Result {
    // may be a dangling reference if the upper 32 bit of metadata_index are 0
    Item& item;

    // lower 32 bit: metadata_index
    // upper 32 bit:
    // - first bit is 0 if failure, 1 if success (FOUND_BIT)
    uint64_t metadata_index_and_flags;
};

/* INTERNALS */

namespace detail {

template<typename Item>
union EmptyReferencePun {
    size_t zero;

    struct Pun {
        Item& item;
    } pun;
};

template<typename Item>
YAHT_INLINE Result<Item> empty_result() {
    EmptyReferencePun<Item> empty = EmptyReferencePun<Item>{0};

    return {empty.pun.item, UINT32_MAX};
}

template<typename Item, typename Hash, typename Pred>
YAHT_INLINE Metadata* metadata(Table<Item, Hash, Pred>* table) {
    return reinterpret_cast<Metadata*>(table + 1);
}

YAHT_INLINE Fingerprint fingerprint(size_t hash) {
    constexpr size_t hash_part_mask = (1 << HASH_LOWER_BITS) - 1;

    return FINGERPRINT_DISTANCE_UNIT | static_cast<Fingerprint>(hash & hash_part_mask);
}

YAHT_INLINE uint32_t upper_hash_part(size_t hash) {
    return static_cast<uint32_t>(hash >> 16);
}

template<typename Item, typename Hash, typename Pred>
YAHT_INLINE uint32_t
metadata_index(Table<Item, Hash, Pred>* table, uint32_t upper_hash_part) {
    return upper_hash_part & table->size_mask;
}

template<typename Item, typename Hash, typename Pred>
YAHT_INLINE uint32_t next_metadata_index(Table<Item, Hash, Pred>* table, uint32_t index) {
    return (index + 1) & table->size_mask;
}

} // namespace detail

// clang-format off
#define YAHT_FOREACH(TABLE, NODE_VAR, INDEX_VAR) \
    if (bool __hack = false); else \
    for (const uint32_t __table_size = (TABLE)->size; !__hack;) \
    for (uint32_t (INDEX_VAR) = 0; !__hack;) \
    for (__hack = true; \
        (INDEX_VAR) < __table_size; \
        ++(INDEX_VAR) \
    ) \
        if (bool __hack2 = false); else \
        for (auto& (NODE_VAR) = (TABLE)->nodes[INDEX_VAR]; !__hack2;) \
        for (; !__hack2; __hack2 = true)

// clang-format on

/* API */

namespace detail {

template<typename Item, typename Hash, typename Pred>
YAHT_INLINE Table<Item, Hash, Pred>*
create_impl(uint32_t size, uint32_t max_size, uint32_t load_factor, Allocator allocator) {
    using TableT = Table<Item, Hash, Pred>;

    // don't check for malloc results:
    // https://lemire.me/blog/2021/10/27/in-c-how-do-you-know-if-the-dynamic-allocation-succeeded/
    auto* table = reinterpret_cast<TableT*>(
        allocator.allocate(sizeof(TableT) + sizeof(Metadata) * max_size)
    );

    table->size = size;
    table->rehash_size = max_size * (static_cast<uint32_t>(load_factor)) / 100;
    table->max_size = max_size;
    table->size_mask = max_size - 1;
    table->load_factor = load_factor;
    table->allocator = allocator;

    Metadata* const metadata = detail::metadata(table);
    for (uint32_t i = 0; i < max_size; ++i) {
        metadata[i].item_index = 0;
        metadata[i].fingerprint = 0;
    }

    return table;
}

} // namespace detail

template<typename Item, typename Hash, typename Pred>
Table<Item, Hash, Pred>*
create(uint32_t max_size, uint32_t load_factor, Allocator allocator) {
    Table<Item, Hash, Pred>* const table =
        detail::create_impl<Item, Hash, Pred>(0, max_size, load_factor, allocator);

    table->nodes = reinterpret_cast<Node<Item>*>(
        allocator.allocate(sizeof(Node<Item>) * (table->rehash_size + 1))
    );

    return table;
}

template<typename Item, typename Hash, typename Pred>
void grow(Table<Item, Hash, Pred>*& old_table, uint32_t power_of_two) {
    using TableT = Table<Item, Hash, Pred>;

    const uint32_t new_max_size = old_table->max_size << power_of_two;
    TableT* const new_table = detail::create_impl<Item, Hash, Pred>(
        old_table->size,
        new_max_size,
        old_table->load_factor,
        old_table->allocator
    );

    detail::Metadata* const metadata = detail::metadata(new_table);
    for (uint32_t i = 0; i < new_max_size; ++i) {
        metadata[i].item_index = 0;
        metadata[i].fingerprint = 0;
    }

    new_table->nodes = reinterpret_cast<Node<Item>*>(old_table->allocator.reallocate(
        old_table->nodes,
        sizeof(Node<Item>) * (new_table->rehash_size + 1)
    ));

    for (uint32_t i = 0; i < old_table->size; i++) {
        Node<Item>& node = new_table->nodes[i];

        const size_t hash = Hash::call(node.item);
        detail::Fingerprint fingerprint = detail::fingerprint(hash);
        const uint32_t upper_hash_part = detail::upper_hash_part(hash);

        uint32_t current_metadata_index =
            detail::metadata_index(new_table, upper_hash_part);

        // like an insert, but:
        // - we're sure the node hasn't been inserted yet
        // - no need to find space for a node since we know its index already

        detail::Metadata current_metadata;
        while (true) {
            current_metadata = metadata[current_metadata_index];
            if (fingerprint > current_metadata.fingerprint) {
                break;
            }

            fingerprint += detail::FINGERPRINT_DISTANCE_UNIT;

            current_metadata_index =
                detail::next_metadata_index(new_table, current_metadata_index);
        }

        metadata[current_metadata_index] = {i, fingerprint};
        node.metadata_index = current_metadata_index;

        detail::Metadata moved_metadata = current_metadata;
        while (moved_metadata.fingerprint != 0) {
            do {
                current_metadata_index =
                    detail::next_metadata_index(new_table, current_metadata_index);
                current_metadata = metadata[current_metadata_index];
                moved_metadata.fingerprint += detail::FINGERPRINT_DISTANCE_UNIT;
            } while (current_metadata.fingerprint >= moved_metadata.fingerprint);

            metadata[current_metadata_index] = moved_metadata;
            moved_metadata = current_metadata;

            new_table->nodes[moved_metadata.item_index].metadata_index =
                current_metadata_index;
        }
    }

    old_table->allocator.deallocate(old_table);
    old_table = new_table;
}

template<typename Item, typename Hash, typename Pred>
void clear(Table<Item, Hash, Pred>* table) {
    table->size = 0;

    detail::Metadata* const metadata = detail::metadata(table);
    for (uint32_t i = 0; i < table->max_size; ++i) {
        metadata[i].item_index = 0;
        metadata[i].fingerprint = 0;
    }
}

template<typename Item, typename Hash, typename Pred>
void destroy(Table<Item, Hash, Pred>* table) {
    table->allocator.deallocate(table->nodes);
    table->allocator.deallocate(table);
}

template<typename Item, typename Hash, typename Pred, typename... Args>
Result<Item> find(Table<Item, Hash, Pred>* table, Args&&... args) {
    // same as the first part of the insert, just unrolled

    const detail::Metadata* const metadata = detail::metadata(table);

    const size_t hash = Hash::call(args...);
    detail::Fingerprint fingerprint = detail::fingerprint(hash);
    const uint32_t upper_hash_part = detail::upper_hash_part(hash);

    uint32_t current_metadata_index = detail::metadata_index(table, upper_hash_part);

    detail::Metadata current_metadata = metadata[current_metadata_index];
    do {
        // try 1
        if (current_metadata.fingerprint == fingerprint) {
            Node<Item>& node = table->nodes[current_metadata.item_index];
            if (YAHT_LIKELY(Pred::call(node.item, args...))) {
                return Result<Item>{node.item, current_metadata_index | FOUND_BIT};
            }
        }
        current_metadata_index =
            detail::next_metadata_index(table, current_metadata_index);
        fingerprint += detail::FINGERPRINT_DISTANCE_UNIT;
        current_metadata = metadata[current_metadata_index];

        // try 2
        if (current_metadata.fingerprint == fingerprint) {
            Node<Item>& node = table->nodes[current_metadata.item_index];
            if (YAHT_LIKELY(Pred::call(node.item, args...))) {
                return Result<Item>{node.item, current_metadata_index | FOUND_BIT};
            }
        }
        current_metadata_index =
            detail::next_metadata_index(table, current_metadata_index);
        fingerprint += detail::FINGERPRINT_DISTANCE_UNIT;
        current_metadata = metadata[current_metadata_index];

        // trying two times before finding out if it makes sense is faster
    } while (fingerprint <= current_metadata.fingerprint);

    return detail::empty_result<Item>();
}

template<typename Item, typename Hash, typename Pred>
YAHT_INLINE Item& acquire(Table<Item, Hash, Pred>* table) {
    return table->nodes[table->size].item;
}

template<typename Item, typename Hash, typename Pred>
Result<Item> insert(Table<Item, Hash, Pred>*& table) {
    detail::Metadata* metadata = detail::metadata(table);
    Node<Item>& inserted_node = table->nodes[table->size];

    const size_t hash = Hash::call(inserted_node.item);
    const detail::Fingerprint original_fingerprint = detail::fingerprint(hash);
    const uint32_t upper_hash_part = detail::upper_hash_part(hash);

    // FIRST PART: try and find if the element is already present

    uint32_t current_metadata_index = detail::metadata_index(table, upper_hash_part);

    // fingerprint the key would have if it was inserted in the current position
    detail::Fingerprint fingerprint = original_fingerprint;

    detail::Metadata current_metadata;
    while (true) {
        current_metadata = metadata[current_metadata_index];
        if (fingerprint > current_metadata.fingerprint) {
            // since fingerprint = distance << X | hash_part:
            // - "fingerprint > current_fingerprint" can be used to compute
            // expected_distance > current distance
            // - if expected_distance > current_distance we won't find the element
            // (see the insertion part for why)
            break;
        }

        Node<Item>& node = table->nodes[current_metadata.item_index];
        if (Pred::call(node.item, inserted_node.item)) {
            return Result<Item>{node.item, current_metadata_index};
        }

        // increase by one the expected distance
        fingerprint += detail::FINGERPRINT_DISTANCE_UNIT;

        // move to next index, wrapping around
        current_metadata_index =
            detail::next_metadata_index(table, current_metadata_index);
    }

    // INTERMEZZO: we know we have to insert, check if we need to grow the table

    // TODO: maybe insert it in a random place and let the rehash do the rest
    if (YAHT_UNLIKELY(table->size == table->rehash_size)) {
        grow(table, 1);

        // reset the fingerprint and the index according to the new size
        fingerprint = original_fingerprint;
        inserted_node = table->nodes[table->size];

        metadata = detail::metadata(table);
        current_metadata_index = detail::metadata_index(table, upper_hash_part);

        // repeat the find above, without checking for the key
        // since we know we won't find it
        while (true) {
            current_metadata = metadata[current_metadata_index];
            if (fingerprint > current_metadata.fingerprint) {
                break;
            }
            fingerprint += detail::FINGERPRINT_DISTANCE_UNIT;
            current_metadata_index =
                detail::next_metadata_index(table, current_metadata_index);
        }
    }

    // SECOND PART: rearrange the table and find the new match

    // change the metadata to point at the new node
    const uint32_t inserted_metadata_index = current_metadata_index;
    metadata[inserted_metadata_index] = {table->size, fingerprint};
    table->size++;

    // set the upper_hash_part on the inserted node
    inserted_node.metadata_index = inserted_metadata_index;

    // if the metadata we displaced points to a node we need to find a new place
    // and we need to do this recursively for each node
    detail::Metadata moved_metadata = current_metadata;
    while (moved_metadata.fingerprint != 0) {
        // find a suitable place for our node starting from the next position
        do {
            current_metadata_index =
                detail::next_metadata_index(table, current_metadata_index);
            current_metadata = metadata[current_metadata_index];
            moved_metadata.fingerprint += detail::FINGERPRINT_DISTANCE_UNIT;
        } while (current_metadata.fingerprint >= moved_metadata.fingerprint);

        // swap the metadata
        metadata[current_metadata_index] = moved_metadata;
        moved_metadata = current_metadata;

        // update the node
        table->nodes[moved_metadata.item_index].metadata_index = current_metadata_index;
    }

    return Result<Item>{inserted_node.item, inserted_metadata_index | FOUND_BIT};
}

namespace detail {

template<typename Item, typename Hash, typename Pred>
YAHT_INLINE void remove_impl(
    Table<Item, Hash, Pred>* table,
    Metadata* metadata,
    uint32_t vacated_metadata_index
) {
    table->size--;

    // find nodes that can be moved back in the vacated positions, recursively
    Metadata* vacated_metadata;
    while (true) {
        vacated_metadata = &metadata[vacated_metadata_index];

        vacated_metadata_index =
            detail::next_metadata_index(table, vacated_metadata_index);
        const Metadata current_metadata = metadata[vacated_metadata_index];

        constexpr uint32_t fingerprint_distance_two_units = 2 * FINGERPRINT_DISTANCE_UNIT;
        if (current_metadata.fingerprint < fingerprint_distance_two_units) {
            // either the node is empty or in its ideal position
            break;
        }

        // update the metadata
        vacated_metadata->fingerprint =
            current_metadata.fingerprint + FINGERPRINT_DISTANCE_UNIT;
        vacated_metadata->item_index = current_metadata.item_index;

        // update the node
        table->nodes[current_metadata.item_index].metadata_index = vacated_metadata_index;
    }

    vacated_metadata->fingerprint = 0;
}

} // namespace detail

template<typename Item, typename Hash, typename Pred>
void remove_from_index(Table<Item, Hash, Pred>* table, uint32_t metadata_index) {
    detail::Metadata* metadata = detail::metadata(table);
    return detail::remove_impl(table, metadata, metadata_index);
}

template<typename Item, typename Hash, typename Pred, typename... Args>
Result<Item> remove(Table<Item, Hash, Pred>* table, Args&&... args) {
    detail::Metadata* const metadata = detail::metadata(table);

    const size_t hash = Hash::call(args...);
    detail::Fingerprint fingerprint = detail::fingerprint(hash);
    const uint32_t upper_hash_part = detail::upper_hash_part(hash);

    uint32_t current_metadata_index = detail::metadata_index(table, upper_hash_part);

    // it's like a find but with a twist when we find the node

    while (true) {
        const detail::Metadata current_metadata = metadata[current_metadata_index];
        if (fingerprint > current_metadata.fingerprint) {
            break;
        }

        Node<Item>& node = table->nodes[current_metadata.item_index];
        if (Pred::call(node.item, args...)) {
            detail::remove_impl(table, metadata, current_metadata_index);
            return Result<Item>{node.item, current_metadata_index | FOUND_BIT};
        }

        fingerprint += detail::FINGERPRINT_DISTANCE_UNIT;
        current_metadata_index =
            detail::next_metadata_index(table, current_metadata_index);
    }

    return detail::empty_result<Item>();
}

// TODO: I still don't like htis
template<typename Item, typename Hash, typename Pred>
YAHT_INLINE void release(Table<Item, Hash, Pred>* table, Item& item) {
    Node<Item>* const last_node = &table->nodes[table->size];
    auto* const node = reinterpret_cast<Node<Item>*>(&item);
    if (YAHT_UNLIKELY(node == last_node)) {
        // avoid copying if the removed node is the last one
        return;
    }
    *node = *last_node;

    detail::Metadata* const metadata = detail::metadata(table);
    detail::Metadata& last_node_metadata = metadata[last_node->metadata_index];

    // make the last node metadata point to the new location
    last_node_metadata.item_index =
        (reinterpret_cast<size_t>(node) - reinterpret_cast<size_t>(table->nodes))
        / sizeof(Node<Item>);
    // std::cerr << reinterpret_cast<size_t>(node) << " "
    //     << reinterpret_cast<size_t>(table->nodes) << " "
    //     << (reinterpret_cast<size_t>(node) - reinterpret_cast<size_t>(table->nodes)) / sizeof(Node<Item>) << " "
    //     << last_node_metadata.item_index << "\n";
}

} // namespace yaht

/* DEFAULTS */

// TODO hash for map nodes only key
#if __STDC_HOSTED__ == 1
#include "stdlib.h"
#include <functional>

namespace yaht {

namespace defaults {

namespace detail {

YAHT_INLINE void* allocate(size_t size) {
    return ::malloc(size);
}

YAHT_INLINE void* reallocate(void* ptr, size_t size) {
    return ::realloc(ptr, size);
}

YAHT_INLINE void deallocate(void* ptr) {
    ::free(ptr);
}

} // namespace detail

struct Pred {
    template<typename T>
    YAHT_INLINE bool call(T a, T b) {
        return a == b;
    }
};

YAHT_ASSERT_IS_POD(Pred);

struct Hash {
    template<typename T>
    YAHT_INLINE size_t call(T key) {
        return std::hash<T>{}(key);
    }
};

YAHT_ASSERT_IS_POD(Hash);

YAHT_ATTRIBUTE((__unused__))
static Allocator allocator = {
    defaults::detail::allocate,
    defaults::detail::reallocate,
    defaults::detail::deallocate,
};

YAHT_ASSERT_IS_POD(Allocator);

} // namespace defaults

} // namespace yaht

#define YAHT_DEFAULT_PRED = defaults::Pred
#define YAHT_DEFAULT_HASH = defaults::Hash
#define YAHT_DEFAULT_ALLOCATOR = defaults::Allocator

#else

#define YAHT_DEFAULT_PRED
#define YAHT_DEFAULT_HASH
#define YAHT_DEFAULT_ALLOCATOR

#endif

namespace yaht {

/* SET */

template<typename K>
struct SetNode {
    K key;
};

YAHT_ASSERT_IS_POD(SetNode<int>);

template<typename K, typename Hash YAHT_DEFAULT_HASH, typename Pred YAHT_DEFAULT_PRED>
class Set : Table<SetNode<K>, Hash, Pred> {};

YAHT_ASSERT_IS_POD(Set<int>);

template<typename K, typename Hash YAHT_DEFAULT_HASH, typename Pred YAHT_DEFAULT_PRED>
YAHT_INLINE Set<K, Hash, Pred>* create_set(size_t size, int load_factor) {
    return create<SetNode<K>, Hash, Pred>(size, load_factor);
}

template<typename K, typename Hash YAHT_DEFAULT_HASH, typename Pred YAHT_DEFAULT_PRED>
YAHT_INLINE Set<K, Hash, Pred>* create_set(size_t size) {
    return create_set<K, Hash, Pred>(size, 75);
}

template<typename K, typename Hash YAHT_DEFAULT_HASH, typename Pred YAHT_DEFAULT_PRED>
YAHT_INLINE Set<K, Hash, Pred>* create_set() {
    return create_set<K, Hash, Pred>(64);
}

/* MAP */

template<typename K, typename V>
struct MapNode {
    K key;
    V value;
};

YAHT_ASSERT_IS_POD(MapNode<int, int>);

template<
    typename K,
    typename V,
    typename Hash YAHT_DEFAULT_HASH,
    typename Pred YAHT_DEFAULT_PRED>
class Map : Table<MapNode<K, V>, Hash, Pred> {};

YAHT_ASSERT_IS_POD(Map<int, int>);

template<
    typename K,
    typename V,
    typename Hash YAHT_DEFAULT_HASH,
    typename Pred YAHT_DEFAULT_PRED>
YAHT_INLINE Map<K, V, Hash, Pred>* create_map(size_t size, int load_factor) {
    return create<MapNode<K, V>, Hash, Pred>(size, load_factor);
}

template<
    typename K,
    typename V,
    typename Hash YAHT_DEFAULT_HASH,
    typename Pred YAHT_DEFAULT_PRED>
YAHT_INLINE Map<K, V, Hash, Pred>* create_map(size_t size) {
    return create_map<K, V, Hash, Pred>(size, 75);
}

template<
    typename K,
    typename V,
    typename Hash YAHT_DEFAULT_HASH,
    typename Pred YAHT_DEFAULT_PRED>
YAHT_INLINE Map<K, V, Hash, Pred>* create_map() {
    return create_map<K, V, Hash, Pred>(64);
}

} // namespace yaht

#undef YAHT_DEFAULT_PRED
#undef YAHT_DEFAULT_HASH
#undef YAHT_DEFAULT_ALLOCATOR
#undef YAHT_ASSERT_IS_POD
#undef YAHT_INLINE
#undef YAHT_LIKELY
#undef YAHT_UNLIKELY

#endif
