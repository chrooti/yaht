# YAHT (yet another hashtable)

Hashset/hashtable library.

### Pros:

* Well commented codebase

* Single header

* FAST 

* Compact (the speed-compactness trade-off is tunable)

* C++11 compatible

### Cons:

* Doesn't have stable nodes

* Non-standard interface

* K and V bigger than sizeof(size_t) are not tested, store a pointer and manage the specifics yourself (see example)

## How to use

`#include "yaht/yaht.hpp"`

## Credits
Benchmark code heavily inspired by https://github.com/martinus/map_benchmark (and the map shares a lot with martinus' robin_hood implementation, thanks Martin!)
