#include <iostream>

#include "yaht.hpp"

struct BufferHash {
    static size_t call(const std::string& buf) {
        return std::hash<std::string>{}(buf);
    }
};

struct BufferPred {
    static bool call(const std::string& a, const std::string& b) {
        return a == b;
    }
};

int main() {
    /* fill some buffers with data */

    constexpr size_t buffer_size = 32;
    constexpr size_t buffer_count = 32;
    std::string buffers[buffer_count];

    for (size_t i = 0; i < buffer_count; i++) {
        new (&buffers[i]) std::string(buffer_size, static_cast<char>('A' + i));
    }

    /* create the hashtable */

    auto* table = yaht::create<std::string, BufferHash, BufferPred>(
        buffer_count,
        75,
        yaht::defaults::allocator
    );

    /* insert the buffers */

    for (size_t i = 0; i < buffer_count; ++i) {
        std::string& buf = yaht::acquire(table);
        new (&buf) std::string(buffers[i]);

        yaht::insert(table);
    }

    /* look them up */

    YAHT_FOREACH (table, node, i) {
        const yaht::Result<std::string> result = yaht::find(table, node.item);
        std::clog << i << ": ";

        if (result.metadata_index_and_flags & yaht::FOUND_BIT) {
            std::clog << "found";
        } else {
            std::clog << "not found";
        }

        std::clog << " (" << node.item << ")" << std::endl;
    }

    /* remove them */

    for (size_t i = 0; i < buffer_count; i++) {
        if (i % 2 == 1) {
            continue;
        }

        const yaht::Result<std::string> result = yaht::remove(table, buffers[i]);
        if (result.metadata_index_and_flags & yaht::FOUND_BIT) {
            yaht::release(table, result.item);
            std::clog << "removed";
        } else {
            std::clog << "not removed";
        }

        std::clog << " (" << buffers[i] << ")" << std::endl;

        // YAHT_FOREACH (table, node, i) {
        //     std::clog << i << " (" << node.item << ")" << "\n";
        // }

        // std::clog << "---------------------" << std::endl;
    }

    return 0;
}
